//
// $Id: SteppingAction.hh 12.07.2016 A. Fijalkowska $
//
/// \file SteppingAction.hh
/// \brief Definition of the SteppingAction class
//
#ifndef SteppingAction_H
#define SteppingACtion_H 1

#include "globals.hh"
#include "G4UserSteppingAction.hh"
#include "G4OpBoundaryProcess.hh"


class SteppingAction : public G4UserSteppingAction
{
  public:

    SteppingAction();
    virtual ~SteppingAction();
    virtual void UserSteppingAction(const G4Step*);
    static G4double energyDep;
 
  private:
     //G4ThreadLocal G4OpBoundaryProcess* FindBoundaryProcess(const G4Step*);
     void PrintStep(const G4Step*, G4OpBoundaryProcessStatus);
     void RecordStep(const G4Step* theStep);
    G4OpBoundaryProcessStatus expectedNextStatus;
};

#endif
