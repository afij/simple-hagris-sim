//
// $Id: EventAction.cc 11.30.2016 A. Fijalkowska $
//
/// \file EventAction.cc
/// \brief Implementation of the EventAction class
//
//
#include "EventAction.hh"
#include "AnalysisManager.hh"

#include "G4EventManager.hh"
#include "G4SDManager.hh"
#include "G4Event.hh"

#include "G4VVisManager.hh"
#include "G4ios.hh"
#include "G4UImanager.hh"
#include "G4SystemOfUnits.hh"

#include "G4THitsMap.hh"
#include "G4HCofThisEvent.hh"

EventAction::EventAction()
{
  HagridEdepCollID2inch = -1;
  HagridEdepCollID3inch = -1;
}
 
EventAction::~EventAction(){}


void EventAction::BeginOfEventAction(const G4Event* anEvent)
{
 
  //New event, add the user information object
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  
  if(HagridEdepCollID2inch < 0)  
    HagridEdepCollID2inch = SDman->GetCollectionID("LaBr3Crystal2Inch/eDep");

  if(HagridEdepCollID3inch < 0)  
    HagridEdepCollID3inch = SDman->GetCollectionID("LaBr3Crystal3Inch/eDep");
}
 

void EventAction::EndOfEventAction(const G4Event* anEvent)
{
	
  G4int eventID = anEvent->GetEventID();
// periodic printing every 100 events
  if( eventID % 100 == 0 )
  {
    G4cout << "Finished Running Event # " << eventID << G4endl;
  }
  
  
  //sensitive detectores
  G4HCofThisEvent* hitsCE = anEvent->GetHCofThisEvent();
  //Get the hit collections
  if(!hitsCE) return;

  AnalysisManager* analysisManager = AnalysisManager::GetInstance();
  G4THitsMap<G4double>* hagridEvtMap2inch = 
			static_cast<G4THitsMap<G4double>*>(hitsCE->GetHC(HagridEdepCollID2inch));

  G4double eDep = 0;
  for(int i=0;i<1;i++)
  { 
    if((*hagridEvtMap2inch)[i]!=0)
    {
			//std::cout << i << " " << *((*hagridEvtMap)[i]) << std::endl;
      eDep = *((*hagridEvtMap2inch)[i]);
      analysisManager->AddHit(eDep, eventID, i);
    }
  }
	


  G4THitsMap<G4double>* hagridEvtMap3ich = 
			static_cast<G4THitsMap<G4double>*>(hitsCE->GetHC(HagridEdepCollID3inch));
  for(int i=0;i<0;i++)
  { 
    if((*hagridEvtMap3ich)[i]!=0)
    {
			//std::cout << i << " " << *((*hagridEvtMap)[i]) << std::endl;
      eDep = *((*hagridEvtMap3ich)[i]);
      analysisManager->AddHit(eDep, eventID, i+25);
    }
  }
	
	
	//analysisManager->AddHit(totEDep, eventID, 19); //id 20 - TAS - like
/*	
	std::map<G4int,G4double*>::iterator itr;
	int i =0;
	for (itr = hagridEvtMap->GetMap()->begin(); itr != hagridEvtMap->GetMap()->end(); itr++)
	{
		//if(*itr!=0)
		{
		    G4double edep = *(itr->second);
		    printf("Collection %d edep %f\n",i,edep);
	    }
		i++;


	}*/
  
}
