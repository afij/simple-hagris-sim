
// $Id: AnalysisManager.cc 12.16.2016 A. Fijalkowska $
//
/// \file AnalysisManager.cc
/// \brief Definition of the AnalysisManager singleton class
//
//
#include "AnalysisManager.hh"
#include "G4SystemOfUnits.hh"
AnalysisManager::AnalysisManager()
{
   rootManager = G4RootAnalysisManager::Instance();	
   rootManager->SetVerboseLevel(1);
   rootManager->SetFirstHistoId(0);
   rootManager->SetFirstNtupleId(0);
   rootManager->SetFirstNtupleColumnId(0);  
   nrOfCreatedTuple = 0;

}

void AnalysisManager::CreateOutput(G4String filename)
{
   rootManager->OpenFile(filename);
   CreateTuple();   
}


void AnalysisManager::SaveOutput()
{
   rootManager->Write();
   rootManager->CloseFile();	
}

void AnalysisManager::CreateTuple()
{
   CreateHagridTuple();
   CreateInitTuple();
}



void AnalysisManager::CreateHagridTuple()
{
   rootManager->CreateNtuple("HagridInfo", "Energy dep in Hagrid");
   rootManager->CreateNtupleIColumn("eventID");
   rootManager->CreateNtupleIColumn("detector_id");
   rootManager->CreateNtupleDColumn("edep");   
   rootManager->FinishNtuple();
   hagridTupleId = nrOfCreatedTuple++;
}

void AnalysisManager::CreateInitTuple()
{
   rootManager->CreateNtuple("InitInfo", "Energy emited in LAB");
   rootManager->CreateNtupleIColumn("eventID");
   rootManager->CreateNtupleDColumn("energy");
   rootManager->CreateNtupleDColumn("xDir"); 
   rootManager->CreateNtupleDColumn("yDir");
   rootManager->CreateNtupleDColumn("zDir");  
   rootManager->CreateNtupleDColumn("theta"); 
   rootManager->FinishNtuple();
   initTupeId = nrOfCreatedTuple++;
}

void AnalysisManager::AddHit(G4double energy, G4int eventId, G4double xDir, G4double yDir, G4double zDir)
{
	int colId = 0;
    rootManager->FillNtupleIColumn(initTupeId, colId, eventId); 
	rootManager->FillNtupleDColumn(initTupeId, ++colId, energy/keV);
	rootManager->FillNtupleDColumn(initTupeId, ++colId, xDir);
	rootManager->FillNtupleDColumn(initTupeId, ++colId, yDir);
	rootManager->FillNtupleDColumn(initTupeId, ++colId, zDir);
	G4double theta = acos(yDir);
	//std::cout << zDir << " " << theta << std::endl;
	rootManager->FillNtupleDColumn(initTupeId, ++colId, theta);
    rootManager->AddNtupleRow(initTupeId);	
}


void AnalysisManager::AddHit(G4double edep, G4int eventId, G4int detId)
{
	int colId = 0;
    rootManager->FillNtupleIColumn(hagridTupleId, colId, eventId); 
	rootManager->FillNtupleIColumn(hagridTupleId, ++colId, detId);
	rootManager->FillNtupleDColumn(hagridTupleId, ++colId, edep/keV);
    rootManager->AddNtupleRow(hagridTupleId);	
}
		
	
AnalysisManager *AnalysisManager::s_instance = 0;
