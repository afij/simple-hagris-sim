//
//
// $Id: PrimaryGeneratorAction.hh 12.10.2016 A Fijalkowska $
//
/// \file PrimaryGeneratorAction.hh
/// \brief Definition of the PrimaryGeneratorAction class

#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4GeneralParticleSource.hh"

class G4ParticleGun;
class G4Event;
class Decay;

class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    PrimaryGeneratorAction();
    virtual ~PrimaryGeneratorAction();

  public:
    virtual void GeneratePrimaries(G4Event*);

  private:
	void SetUpDefault();
	void LoadDecay(std::string filename);
	void GenerateIsotropicDirectionDistribution (G4ThreeVector* direction, 
	                                             G4double thetaMin, 
	                                             G4double thetaMax, 
	                                             G4double phiMin, 
	                                             G4double phiMax);
	void GenerateIsotropicDirectionDistribution(G4ThreeVector* direction, 
	                                            G4double theta0);

	void GenerateSingleParticle(G4Event* anEvent);
	void GenerateDopplerCorrectedEvents(G4Event* anEvent);
	void GenerateDopplerDirectionDistribution
                             (G4ThreeVector* direction, 
                             G4double* energy, 
                             G4double gamma);

                  
    void GenerateDecay(G4Event* anEvent);
    G4double FindGamma();

    G4ParticleGun* particleGun;
    G4GeneralParticleSource* particleSource;
    Decay* decay;
    bool runDecay;
};


#endif 

