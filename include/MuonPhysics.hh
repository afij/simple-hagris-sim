//
// $Id: MuonPhysics.hh 2016-15-11 A Fijalkowska $
//
/// \file MuonPhysics.hh
/// \brief Definition of the MuonPhysics class
/// Class taken from example optical/LXe/
//
//
#ifndef MuonPhysics_h
#define MuonPhysics_h 1

#include "globals.hh"
#include "G4ios.hh"

#include "G4VPhysicsConstructor.hh"


class MuonPhysics : public G4VPhysicsConstructor
{
  public:

    MuonPhysics(const G4String& name="muon");
    virtual ~MuonPhysics();

    // This method will be invoked in the Construct() method.
    // each particle type will be instantiated
    virtual void ConstructParticle();
 
    // This method will be invoked in the Construct() method.
    // each physics process will be instantiated and
    // registered to the process manager of each particle type
    virtual void ConstructProcess();

};

#endif
