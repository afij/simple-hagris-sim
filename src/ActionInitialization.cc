//
// $Id: ActionInitialization.cc 12.10.2016 A Fijalkowska $
//
/// \file ActionInitialization.cc
/// \brief Implementation of the ActionInitialization class

#include "ActionInitialization.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"
#include "EventAction.hh"
#include "SteppingAction.hh"
//#include "SteppingVerbose.hh"



ActionInitialization::ActionInitialization()
 : G4VUserActionInitialization()
{}


ActionInitialization::~ActionInitialization()
{}


void ActionInitialization::BuildForMaster() const
{
	SetUserAction(new RunAction());
}


void ActionInitialization::Build() const
{
	SetUserAction(new PrimaryGeneratorAction());
	SetUserAction(new RunAction());
	SetUserAction(new EventAction());
	SetUserAction(new SteppingAction());
}


/*G4VSteppingVerbose*
               ActionInitialization::InitializeSteppingVerbose() const
{
  return new SteppingVerbose();
}  */
