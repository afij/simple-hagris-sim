
//
// $Id: PrimaryGeneratorAction.cc 12.10.2016 A Fijalkowska $
//
/// \file PrimaryGeneratorAction.cc
/// \brief Implementation of the PrimaryGeneratorAction class

#include "PrimaryGeneratorAction.hh"
#include "AnalysisManager.hh"
#include "Randomize.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Decay.hh"
#include "Exception.hh"

#include "G4GeneralParticleSource.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
 : G4VUserPrimaryGeneratorAction(), 
   particleGun(0)
{
    G4int n_particle = 1;
    particleGun = new G4ParticleGun(n_particle);
    particleSource = new G4GeneralParticleSource();
    runDecay = true;
	//SetUpDefault();

}


PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete particleGun;
}

void PrimaryGeneratorAction::SetUpDefault()
{
	G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
	G4ParticleDefinition* particle = particleTable->FindParticle("gamma");

	particleGun->SetParticleDefinition(particle);
	particleGun->SetParticleTime(0.0*ns);
	particleGun->SetParticlePosition(G4ThreeVector(0.0*cm,0.0*cm,0.0*cm));
	particleGun->SetParticleMomentumDirection(G4ThreeVector(1.,0.,0.));
	particleGun->SetParticleEnergy(500.0*keV);
	
	if(runDecay)
		LoadDecay("Isotope.ens");
		
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{	

    //GenerateDecay(anEvent);
    particleSource->GeneratePrimaryVertex(anEvent);
}	

void PrimaryGeneratorAction::GenerateSingleParticle(G4Event* anEvent)
{

	G4ThreeVector aim(0.0,1.0,0.0);
	GenerateIsotropicDirectionDistribution(&aim,0.0);
    
	G4ThreeVector startPos(0.0*cm,0.0*cm,0.0*cm);	
	particleGun->SetParticlePosition(startPos);
	particleGun->SetParticleMomentumDirection(aim);
	particleGun->GeneratePrimaryVertex(anEvent);

	return;
}
	
void PrimaryGeneratorAction::GenerateDopplerCorrectedEvents(G4Event* anEvent)
{

	G4ThreeVector aim(0.0,1.0,0.0);
	GenerateIsotropicDirectionDistribution(&aim,0.0);
	
	G4double gamma = FindGamma();	
	G4int event_id = anEvent->GetEventID();
	G4double energy = 711*keV;	
	GenerateDopplerDirectionDistribution (&aim, &energy,gamma);	
    particleGun->SetParticleEnergy(energy);
    
	G4ThreeVector startPos(0.0*cm,0.0*cm,0.0*cm);	
	particleGun->SetParticlePosition(startPos);
	particleGun->SetParticleMomentumDirection(aim);
	particleGun->GeneratePrimaryVertex(anEvent);
	
	AnalysisManager* analysisManager = AnalysisManager::GetInstance();	  
    analysisManager->AddHit(energy, event_id, aim[0], aim[1], aim[2]);
	return;
}

void PrimaryGeneratorAction::GenerateDecay(G4Event* anEvent)
{

	
	G4ThreeVector startPos( 0.*mm, 0.0*cm, 0.0*cm );
	
	//Direction
	G4ThreeVector direction( 0.0, 1.0, 0.0 );
	GenerateIsotropicDirectionDistribution(&direction,0.0);
	std::vector<Event> allDecay;
	try{
		allDecay = decay -> Execute();
	}
	catch (Exception& except)
	{
		std::cout << "PrimaryGeneratorAction exception: " 
		<< except.GetMessage() << std::endl;
		throw except;
	}
	
	for(unsigned int i=0; i<allDecay.size();i++)
	{
			particleGun->SetParticlePosition( startPos );
			GenerateIsotropicDirectionDistribution(&direction,0.0);
			particleGun->SetParticleMomentumDirection( direction );
			particleGun->SetParticleEnergy(allDecay.at(i).energy *keV);
			particleGun->SetParticleDefinition(allDecay.at(i).type);
			particleGun->GeneratePrimaryVertex(anEvent);
	}
}

void PrimaryGeneratorAction::LoadDecay(std::string filename)
{  
	try
	{
		decay=new Decay(filename);
	}
	catch (Exception& except)
	{
		std::cout << "PrimaryGeneratorAction::LoadDecay exception: " 
		<< except.GetMessage() << std::endl;
		throw except;
	}
}


void PrimaryGeneratorAction::GenerateIsotropicDirectionDistribution
                             (G4ThreeVector* direction,
                             G4double thetaMin, 
                             G4double thetaMax, 
                             G4double phiMin, 
                             G4double phiMax)
{
	G4double cosThetaMin = cos(thetaMin);
	G4double cosThetaMax = cos(thetaMax);
	
	//assumption: cosThetaMin > cosThetaMax 
	G4double randomCosTheta = G4UniformRand()*(cosThetaMin-cosThetaMax) + cosThetaMax;
	G4double randomSinTheta = sqrt(1.0 - randomCosTheta * randomCosTheta);
	G4double randomPhi = G4UniformRand()*(phiMax-phiMin) + phiMin;

	*direction = G4ThreeVector(cos(randomPhi)*randomSinTheta, 
	                           sin(randomPhi)*randomSinTheta, 
	                           randomCosTheta);	 
}


void PrimaryGeneratorAction::GenerateIsotropicDirectionDistribution
                             (G4ThreeVector* direction, G4double theta0)
{
	
	                                        
	// -cos(Theta0) < cosTheta < cos(Theta0)
	G4double cosTheta = ( G4UniformRand() - 0.5 ) * 2.0*cos(theta0);
	G4double sinTheta = sqrt( 1.0 - cosTheta * cosTheta );
	// 0. < phi < 2*pi
	G4double phi = G4UniformRand() * 8.0 * atan(1.0);
	*direction = G4ThreeVector (cos(phi)*sinTheta, sin(phi)*sinTheta, cosTheta);
}


void PrimaryGeneratorAction::GenerateDopplerDirectionDistribution
                             (G4ThreeVector* direction, 
                             G4double* energy, 
                             G4double gamma)
{
	
	G4ThreeVector randomDirection = G4ThreeVector(0,0,1);
	GenerateIsotropicDirectionDistribution(&randomDirection, 0);

	//std::cout << "was " << randomDirection << " " << *energy   ;
	G4double beta = std::sqrt(1.-1./(gamma*gamma));
    //std::cout << " beta " << beta << std::endl;
	G4double energyTemp = *energy;
	
	//find new energy 
	*energy = gamma*energyTemp + gamma*beta*randomDirection[2]*energyTemp;
	G4ThreeVector momentuDirection = randomDirection*energyTemp;
	//std::cout << " momentum was " << momentuDirection << std::endl;
	momentuDirection[2] = gamma*beta*energyTemp+ gamma*momentuDirection[2];
	//std::cout << " momentum is " << momentuDirection << std::endl;
	
	*direction = momentuDirection/(*energy);
	

	//std::cout << " dir" << *direction << " en " << *energy << std::endl;

	
	
}
 
                

G4double PrimaryGeneratorAction::FindGamma()
{
	
   G4double kineticEnergy = 40; //in MeV/u

   G4double protonMass = 938;
   G4double nrOfNucleons = 80;

	G4double restEnergy = nrOfNucleons*protonMass;
	G4double totKineticEnergy = kineticEnergy*nrOfNucleons;
	G4double totalEn = restEnergy + totKineticEnergy;
	//std::cout << "gamma " << totalEn/restEnergy << std::endl;
	return totalEn/restEnergy;
	
}






