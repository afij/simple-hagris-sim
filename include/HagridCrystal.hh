#ifndef HAGRIDCRYSTAL_H
#define HAGRIDCRYSTAL_H

#include "G4Material.hh"

#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"

#include "G4LogicalVolume.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4OpticalSurface.hh"

class HagridCrystal {
	public:
		static HagridCrystal* Get2inchCrystal();
		static HagridCrystal* Get3inchCrystal();
		virtual ~HagridCrystal();

		void Place(G4LogicalVolume* world,int copyNr, const G4ThreeVector &pos, const G4RotationMatrix &rotation = G4RotationMatrix(0,0,0));
        void ConstructSDandField();
		G4LogicalVolume* GetLeBr3CrystalLogic() {return logicLaBr3Crystal;}

	private:
	    HagridCrystal();
		void BuildMaterials();
		void Construct();	
	    
		bool useOptical_;
		bool checkOverlaps_;
		G4Material *air_;
		G4Material *teflon_;
		G4Material *aluminum_;
		G4Material *glass_;
		G4Material *laBr3_Ce_;
		G4Material *pmtMaterial_;
		G4OpticalSurface *teflonSurface_;
		G4OpticalSurface* pmtSurface_;
		G4double detectorHeight;
		
		G4LogicalVolume* logicLaBr3Crystal;
		G4LogicalVolume* detectorLogic;
		
		G4double alumHeight;
	    G4double alumRadius;

	    G4double crystalHeight;
	    G4double crystalRadius;
	    G4String SDName;
	
		G4MultiFunctionalDetector* cryst;
};

#endif //HAGRIDCRYSTAL_H
