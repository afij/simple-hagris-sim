

#ifndef DetectorConstruction_H
#define DetectorConstruction_H 1

// GEANT4 //
class G4VSolid;
class G4LogicalVolume;
class G4VPhysicalVolume;

#include "G4Material.hh"
#include "G4Element.hh"
#include "G4ThreeVector.hh"
#include "G4VUserDetectorConstruction.hh"
#include "HagridCrystal.hh"


class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:

  DetectorConstruction();
  ~DetectorConstruction();

  virtual G4VPhysicalVolume* Construct();
  virtual void ConstructSDandField();
  private:
  void DefineMaterials();
  G4VSolid * world_solid;
  G4LogicalVolume* world_logical;
  G4VPhysicalVolume* world_physical;



  G4double world_size;
  G4double hagridXpos;
  G4double hagridDiameter;
  G4Material *world_mat;
  HagridCrystal*hagrid2inchCry;
  HagridCrystal*hagrid3inchCry;
  G4double inch;

};

#endif

