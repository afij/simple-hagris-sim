//
// $Id: EventAction.hh 11.30.2016 A. Fijalkowska $
//
/// \file EventAction.hh
/// \brief Definition of the EventAction class
//

#ifndef EventAction_h
#define EventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"

class G4Event;

class EventAction : public G4UserEventAction
{
  public:

    EventAction();
    virtual ~EventAction();

  public:

    virtual void BeginOfEventAction(const G4Event*);
    virtual void EndOfEventAction(const G4Event*);

  private:
    G4int  HagridEdepCollID2inch;
    G4int  HagridEdepCollID3inch;
};

#endif
