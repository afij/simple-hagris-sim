// $Id: DetectorConstruction.cc 03.02.2016 A Fijalkowska $
//
/// \file DetectorConstruction.cc
/// \brief Implementation of the DetectorConstruction, Travis code
//
//

// USER //
#include "DetectorConstruction.hh"

// CADMESH //
#include "CADMesh.hh"

// GEANT4 //
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4SDManager.hh"

#include "G4Material.hh"
#include "G4Isotope.hh"
#include "G4Element.hh"
#include "G4VisAttributes.hh"

#include "G4PhysicalConstants.hh"
#include "MaterialsManager.hh"

DetectorConstruction::DetectorConstruction()
  :world_solid(0), world_logical(0), world_physical(0)
{
  
  world_size = 0.5*m;
  inch = 2.54*cm;
  hagridXpos = 5*inch;
  hagridDiameter = 81.2*mm;
  
}

DetectorConstruction::~DetectorConstruction()
{
}


void DetectorConstruction::DefineMaterials()
{
    // Make sure to call this function!!!  
    MaterialsManager* materialsManager = MaterialsManager::GetInstance();
	// materials 
    world_mat = materialsManager->GetAir();
  
}


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  DefineMaterials();
  // +y-axis is up, +z-axis is beam direction, +x axis is beam-left

  // define world
  world_solid = new G4Box("world", world_size, world_size, world_size);
  world_logical = new G4LogicalVolume(world_solid, world_mat,"world");
  world_physical = new G4PVPlacement(0, G4ThreeVector(), world_logical, 
                                     "world", 0, false, 0);
                                     


//HAGRID
  hagrid2inchCry = HagridCrystal::Get2inchCrystal();
  hagrid3inchCry = HagridCrystal::Get3inchCrystal();

  G4ThreeVector hagPos(0, 0, 5*cm);
  int nrOf3inchCryCopy = 0;
  int nrOf2inchCryCopy = 0;
  
  G4RotationMatrix rotation = G4RotationMatrix(0,0,0);
  hagrid2inchCry->Place(world_logical, nrOf2inchCryCopy++, hagPos, rotation);
	  

  
  return world_physical;
}



void DetectorConstruction::ConstructSDandField() 
{

  //orrubaDet->ConstructSDandField();
  hagrid3inchCry->ConstructSDandField();
  hagrid2inchCry->ConstructSDandField();
}
