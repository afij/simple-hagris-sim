// $Id: AnalysisManager.hh 12.08.2016 A. Fijalkowska $
//
/// \file AnalysisManager.hh
/// \brief Definition of the AnalysisManager singleton class
///

#ifndef AnalysisManager_h
#define AnalysisManager_h 1

#include "G4RootAnalysisManager.hh"
#include "G4String.hh"

class AnalysisManager
{
	public:

		void CreateOutput(G4String filename);
		void SaveOutput();
		void AddHit(G4double edep, G4int eventId, G4int detId = 0);
		void AddHit(G4double energy, G4int eventId, G4double xDir, G4double yDir, G4double zDir);
		
	private:
		AnalysisManager();
		virtual ~AnalysisManager() {}
		void CreateTuple();
		void CreateHagridTuple();
		void CreateInitTuple();
		
		static AnalysisManager *s_instance;
		G4RootAnalysisManager* rootManager;
		G4int nrOfCreatedTuple;		
		G4int hagridTupleId;
		G4int initTupeId;
		
		
	public:
	static AnalysisManager* GetInstance()
	{
		if (!s_instance)
			s_instance = new AnalysisManager();
		return s_instance;
	}
		
};

#endif
